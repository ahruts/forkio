###  FORKIO LANDING

Landing page for web editor adapted for all devices.

The layout - https://www.figma.com/file/9lLwBJciU4yjDZBSnqqXSS/Forkio?node-id=0%3A1

Used tools: SCSS, Gulp, Fontawesome
